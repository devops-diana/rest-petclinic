pipeline {
    agent none
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                }
            }
            steps {
                sh 'mvn clean package -B -ntp'
            }
        }
        stage('Testing') {
            agent any
            steps {
                jacoco()
            }
        }
        stage('Sonar') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                }
            }
            steps {
                withCredentials([file(credentialsId: 'settings-sonar', variable: 'CONFIG_SETTINGS')]) {
                    withSonarQubeEnv(installationName: 'SonarAnalysis') {
                        sh "mvn sonar:sonar -B -ntp -s ${CONFIG_SETTINGS}"
                    }
                }
            }
        }
        stage('QualityGate') {
            agent any
            steps {
                timeout(time: 4, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Deploy-DockerHub') {
            agent any
            steps {
                script{
                    def pom = readMavenPom file: 'pom.xml'
                    def app = docker.build("dianadev620/${pom.artifactId}:${pom.version}")

                    docker.withRegistry('https://registry.hub.docker.com', 'dockerhub-keys'){
                        app.push()
                        app.push('latest')
                    }

                }
            }
        }
    }
}
