# Proyecto Final Curso Mitocode - Arquitecto Devops
Tipo de Arquitectura: Microservicio

Se presenta una pipeline de Jenkins que pasa por los siguientes stages:

## Build

```bash
       stage('Build') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                }
            }
            steps {
                sh 'mvn clean package -B -ntp'
            }
        }
```
## Testing

```bash
       stage('Testing') {
            agent any
            steps {
                jacoco()
            }
        }
```
## Sonar

```bash
       stage('Sonar') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                }
            }
            steps {
                withCredentials([file(credentialsId: 'settings-sonar', variable: 'CONFIG_SETTINGS')]) {
                    withSonarQubeEnv(installationName: 'SonarAnalysis') {
                        sh "mvn sonar:sonar -B -ntp -s ${CONFIG_SETTINGS}"
                    }
                }
            }
        }
```
## QualityGate

```bash
       stage('QualityGate') {
            agent any
            steps {
                timeout(time: 4, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
```
## Artifactory

```bash
       stage('Artifactory') {
            agent any
            steps {
                script {
                    def pom = readMavenPom file: 'pom.xml'
                    def pomVersion = pom.version
                    def server = Artifactory.server 'artifactory-server'
                    def repository = 'spring-petclinic-mono-'

                    if(pomVersion.toLowerCase().contains('release')){
                        repository = repository + 'release'
                    } else {
                        repository = repository + 'snapshot'
                    }

                    def uploadSpec = """
                        {
                            "files": [
                                {
                                    "pattern": "target/.*.jar",
                                    "target": "${repository}",
                                    "regexp": "true"
                                }
                            ]
                        }
                    """
                    server.upload spec: uploadSpec
                }
            }
        }
```
## Deploy-DockerHub

```bash
       stage('Deploy-DockerHub') {
            agent any
            steps {
                script{
                    def pom = readMavenPom file: 'pom.xml'
                    def app = docker.build("dianadev620/${pom.artifactId}:${pom.version}")

                    docker.withRegistry('https://registry.hub.docker.com', 'dockerhub-keys'){
                        app.push()
                        app.push('latest')
                    }

                }
            }
        }
```
